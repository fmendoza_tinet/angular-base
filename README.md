# Angular BASE

Este es un ejemplo simple de como manejar los componentes de una aplicación angular.
El ejemplo esta formado por 2 componentes web: listado y edición. Estos componentes están desplegados en la misma pantalla.

Además se agrega un servicio REST básico, que se encarga de hacer el manejo de estas peticiones y mantener las entidades en memoria.

A nivel de estructura de capertas en el proyecto angular base se maneja lo siguiente:

- componentes (componentes visuales de la aplicación).
- model (Modelo de datos de la aplicación).
- servicios (Llamadas a los servicios REST o lógica externa al despliegue).
- utils (Utilitarios que no son lógica interna ni lógica visual).

En el proyecto servicio express, se maneja lo siguiente:

- server.js (Servicio con la implementacion de las llamadas REST).

## Instrucciones de uso

Para ejecutar la aplicacion, primero debe ejecutarse el servicio:

```console
cd pandero-dummy/
npm install
node server.js
```

Después, **en otra consola**, se debe ejecutar la aplicacion angular en si.

```console
cd ginni-base/
npm install
npm install -g @angular/cli
ng serve --open
```

## Documentación
Se agrega documentacion en de angular.
Para generar la documentacion se debe ejecutar el comando:

```console
cd ginni-base/
npm run compodoc
```
