var express = require('express'), bodyParser = require('body-parser')
let app = express(),
    port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.listen(port);

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  next();
});
// Agregar valores por defecto para poder entregar un listado.
var contrato = { "numeroContrato": "1000-001-01", "idContrato": "1", "nombreProducto": "Producto123", "fechaCreacion": Date.now };
var contrato2 = { "numeroContrato": "1001-002-01", "idContrato": "2", "nombreProducto": "Producto23", "fechaCreacion": Date.now };

// Obtener un contrato a partir del id.
app.get('/contrato/:idContrato', (req, res) => {
  if (req.params.idContrato == "1") {
    res.send(contrato);
  } else {
    res.send(contrato2)
  }
});

// Enviar un listado de contratos.
app.get('/contrato/', (req, res) => {
  res.send([contrato, contrato2]);
});

// Actualizar los datos de un contrato.
app.put('/contrato/:idContrato', (req, res) => {
  if (req.params.idContrato == "1") {
    contrato = req.body
  } else {
    contrato2 = req.body
  }
  console.log(contrato);
  res.send(req.body)
});

console.log("Server running at http://localhost:" + port + "/");
