import { BaseComponent } from './componentes/base/base.component';
import { Base2Component } from './componentes/base2/base2.component';
import { TokenerrorComponent } from './componentes/tokenerror/tokenerror.component';
import { AuthService } from './servicios/secure/auth.service';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**
 * Rutas de la aplicación.
 */
const appRoutes: Routes = [
      { path: '', component: BaseComponent },
      { path: 'base', component: BaseComponent, canActivate: [AuthService] },
      { path: 'base2', component: Base2Component, canActivate: [AuthService] },
      { path: 'tokenerror', component: TokenerrorComponent },
];

/**
 * Listado de proveedores de enrutamiento.
 */
export const appRoutingProviders: any[] = [];

/**
 * Mapeo de modulos con proveedores.
 */
export const routing: ModuleWithProviders = RouterModule.forRoot(
      appRoutes, {
            useHash: Boolean(history.pushState) === false
      });

