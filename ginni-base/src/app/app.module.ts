import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { appRoutingProviders, routing } from './app.routing';
import { AppComponent } from './app.component';
import { BaseComponent } from './componentes/base/base.component';
import { Base2Component } from './componentes/base2/base2.component';
import { TokenerrorComponent } from './componentes/tokenerror/tokenerror.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    Base2Component,
    TokenerrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
