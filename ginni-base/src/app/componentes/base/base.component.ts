import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../servicios/base/base.service';
import { Funciones } from './../../utils/funciones';
import { Contrato } from '../../model/contrato';

/**
 * Aplicación base.
 */
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  /** @ignore */
  constructor(private baseService: BaseService) { }

  /**
   * Listado de contratos.
   */
  contratos = [];

  /**
   * Contrato seleccionado.
   */
  contratoSelecionado;

  /** @ignore */
  ngOnInit() {
    this.metodoObtenerListado();
  }

  /**
   * Obtener los datos base de un producto usando el código.
   * @param codigoProducto Codigo del producto.
   */
  metodoObtener(codigoProducto: string) {
    this.baseService.metodoObtenerSimple(codigoProducto).subscribe((data: Contrato) => {
      Funciones.log(data);
    });
  }

  /**
   * Obtener un producto usando el codigo de producto.
   * @param codigoProducto Codigo del producto.
   */
  metodoObtenerFull(codigoProducto: string) {
    this.baseService.metodoObtenerFull(codigoProducto).subscribe(resp => {
      Funciones.log('Headers:' + JSON.stringify(resp.headers));
      Funciones.log('Body:' + JSON.stringify(resp.body));
    });
  }

  /**
   * Obtener un listado de contratos.
   */
  metodoObtenerListado() {
    this.baseService.metodoObtenerListado().subscribe((data: Contrato[]) => {
      Funciones.log(data);
      this.contratos = data;
    });
  }

  /**
   * Seleccionar un contrato.
   */
  cambiarContratoSelecionado() {
    Funciones.log(this.contratoSelecionado);
  }

  /**
   * Actualizar el listado de contratos.
   * */
  actualizar() {
    this.metodoObtenerListado();
  }
}
