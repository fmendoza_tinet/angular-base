import { Component, OnInit } from '@angular/core';

/**
 * Error al obtener token.
 */
@Component({
  selector: 'app-tokenerror',
  templateUrl: './tokenerror.component.html',
  styleUrls: ['./tokenerror.component.css']
})
export class TokenerrorComponent implements OnInit {

  /** @ignore */
  constructor() { }

  /** @ignore */
  ngOnInit() {
  }

}
