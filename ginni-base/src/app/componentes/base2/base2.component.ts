import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Contrato } from '../../model/contrato';
import { Funciones } from '../../utils/funciones';
import { BaseService } from '../../servicios/base/base.service';

/**
 * Segunda parte aplicacion base
 */
@Component({
  selector: 'app-base2',
  templateUrl: './base2.component.html',
  styleUrls: ['./base2.component.css']
})
export class Base2Component implements OnInit {
  /**
   * Contrato a desplegar.
   */
  contrato: Contrato;

  /** @ignore */
  constructor(private baseService: BaseService) { }

  /**
   * Emisor encargado de actualizar los datos en la pantalla.
   */
  @Output() actualizar = new EventEmitter();

  /**@ignore */
  ngOnInit() {
    this.contrato = null;
  }

  /**
   * Identificador del contrato.
   */
  @Input()
  set idContrato(identificador: string) {
    if (identificador) {
      this.baseService.metodoObtenerSimple(identificador).subscribe((data) => {
        this.contrato = data;
      });
    }
  }

  /**
   * Actualizar los datos de un contrato.
   */
  public actualizarContrato() {
    this.baseService.metodoActualizar(this.contrato).subscribe((data) => {
      this.contrato = data;
      this.actualizar.emit();
    });
  }
}
