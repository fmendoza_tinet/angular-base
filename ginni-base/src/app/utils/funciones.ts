import { environment } from '../../environments/environment';

/**
 * Funciones utilitarias para la aplicacion.
 */
export class Funciones {
    /**
     * Metodo encargado de loguear o no segun varible de produccion..
     * @param val texto para logear.
     */
    public static log(val) {
        if (!environment.production) {
            console.log(val);
        }
    }
}
