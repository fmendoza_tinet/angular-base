import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, } from 'rxjs';
import { Contrato } from '../../model/contrato';

/** Ubicacion donde desplegar el servicio */
@Injectable({
  providedIn: 'root'
})
export class BaseService {

  /** URL del endpoint. */
  private urlServicio = environment.urlServicio;

  /** @ignore */
  constructor(private http: HttpClient) { }

  /** Obtener los datos basicos de un contrato. */
  public metodoObtenerSimple(idContrato: string): Observable<Contrato> {
    return this.http.get<Contrato>(this.urlServicio + environment.endpointContrato + idContrato);
  }

  /**
   * Obtener los datos de un cotrato.
   * @param idContrato Identificador de contrato.
   */
  public metodoObtenerFull(idContrato: string): Observable<HttpResponse<Contrato>> {
    return this.http.get<Contrato>(this.urlServicio + environment.endpointContrato + idContrato, { observe: 'response' });
  }

  /**
   * Obtener un listado de contratos.
   */
  public metodoObtenerListado(): Observable<Contrato[]> {
    return this.http.get<Contrato[]>(this.urlServicio + environment.endpointContrato);
  }

  /**
   * Actualizar los datos de un contrato.
   */
  public metodoActualizar(contrato: Contrato): Observable<Contrato> {
    return this.http.put<Contrato>(this.urlServicio + environment.endpointContrato + contrato.idContrato, contrato);
  }
}
