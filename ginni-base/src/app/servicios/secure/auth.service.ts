import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {

  constructor(private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    // TODO debe definirse con la arquitectura de segurizacion.
    if (route.routeConfig.path === 'base') {
      return Observable.create(observer => {
        this.router.navigate(['tokenerror']);
        observer.next(false);
        observer.complete();
      });
    }

    return Observable.create(observer => {
      observer.next(true);
      observer.complete();

    });
  }
}
