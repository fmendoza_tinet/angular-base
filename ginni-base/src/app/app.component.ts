import { Component } from '@angular/core';

/**
 * Selector base de la aplicación.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  /** Título de la aplicación */
  title = 'ginni-base';
}
