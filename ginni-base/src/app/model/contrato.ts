/**
 * Contrato.
 */
export class Contrato {
    /**
     * Numero de comtrato.
     */
    numeroContrato: string;

    /**
     * Identificador del contrato.
     */
    idContrato: string;

    /**
     * Nombre del producto contratado.
     */
    nombreProducto: string;
}
