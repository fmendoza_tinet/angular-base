/**
 * Configuracion utilizada en el ambiente productivo.
 */
export const environment = {
  production: true,
  urlServicio: 'http://localhost:3000/',
  endpointContrato: 'contrato/'
};
